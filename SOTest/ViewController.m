//
//  ViewController.m
//  SOTest
//
//  Created by Abizer Nasir on 26/12/2014.
//  Copyright (c) 2014 Jungle Candy Software Ltd. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UIView *greenView;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [self setupWidthConstraints];
}

- (void)setupWidthConstraints {
    NSLayoutConstraint *widthConstraint;
    widthConstraint = [NSLayoutConstraint constraintWithItem:self.greenView
                                                   attribute:NSLayoutAttributeWidth
                                                   relatedBy:NSLayoutRelationEqual
                                                      toItem:self.view
                                                   attribute:NSLayoutAttributeWidth
                                                  multiplier:0.30
                                                    constant:0.0];
    
    [self.view addConstraint:widthConstraint];
}

@end
